#include "openrgbhub.hpp"
#include "usbmodule_arduinohid.hpp"
#include "fastled_zone.hpp"
#include "test_effect.hpp"
#include "arduino_analog_zone.hpp"
#include "arduino_pwm_fan.hpp"
#include "arduino_temperature_sensor.hpp"
#include "hub_layout.hpp"
#include "utils.hpp"
#include <Arduino.h>

FastLEDZone<DATA_PIN_CHANNEL_1, 25> zone_1("ARGB Zone 1");
FastLEDZone<DATA_PIN_CHANNEL_2, 25> zone_2("ARGB Zone 2");
FastLEDZone<DATA_PIN_CHANNEL_3, 25> zone_3("ARGB Zone 3");
FastLEDZone<DATA_PIN_CHANNEL_4, 25> zone_4("ARGB Zone 4");
FastLEDZone<DATA_PIN_CHANNEL_5, 25> zone_5("ARGB Zone 5");
FastLEDZone<DATA_PIN_CHANNEL_6, 25> zone_6("ARGB Zone 6");

Zone * zone_array[] = 
{
  &zone_1,
  &zone_2,
  &zone_3,
  &zone_4,
  &zone_5,
  &zone_6
};

effect_constructor_t * effect_list[] = 
{
  &TestEffectConstructor
};

RGBManager rgb_manager(zone_array, 
                       ARRSIZE(zone_array),          
                       effect_list, 
                       ARRSIZE(effect_list)
                      );

ArduinoHIDUSBModule hid_module;

ArduinoPWMFan fan_1(LEONARDO_PIN_6, FAN_RPM_PIN_1, "Arduino fan 1");
ArduinoPWMFan fan_2(LEONARDO_PIN_10, FAN_RPM_PIN_2, "Arduino fan 2");

Fan * fan_array[]
{
  &fan_1,
  &fan_2
};

ArduinoTempSensor sensor_1(TEMP_SENSOR_PIN_1, 10000, 100000, 4242.0, "Temp Sensor 1");
ArduinoTempSensor sensor_2(TEMP_SENSOR_PIN_2, 10000, 100000, 4242.0, "Temp Sensor 2");
ArduinoTempSensor sensor_3(TEMP_SENSOR_PIN_3, 10000, 100000, 4242.0, "Temp Sensor 3");
ArduinoTempSensor sensor_4(TEMP_SENSOR_PIN_4, 10000, 100000, 4242.0, "Temp Sensor 4");

Sensor * sensor_array[]
{
  &sensor_1,
  &sensor_2,
  &sensor_3,
  &sensor_4
};

SensorManager sensor_manager = SensorManager(sensor_array, ARRSIZE(sensor_array));

FanManager fan_manager = FanManager(fan_array, ARRSIZE(fan_array), &sensor_manager);

OpenRGBHub hub = OpenRGBHub("OpenRGB Hub", &hid_module, &rgb_manager, &fan_manager, &sensor_manager);

/* Arduino sketches use setup() and loop() instead of main() */
void setup() {
  hub.begin();
  Serial.begin(9600);
  pinMode(LED_BUILTIN_TX,INPUT);
  pinMode(LED_BUILTIN_RX,INPUT);
}

void loop() {
  hub.loop();
}
