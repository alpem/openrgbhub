#include "sensormanager.hpp"

SensorManager::SensorManager(Sensor ** sensor_list, uint8_t sensor_list_size)
{
    this->sensor_list = sensor_list;
    this->sensor_list_size = sensor_list_size;
}

void SensorManager::begin()
{
    for (int i = 0; i < sensor_list_size; i++)
    {
        sensor_list[i]->begin();
    }
}

uint8_t SensorManager::getSensorNum()
{
    return sensor_list_size;
}

uint16_t SensorManager::readSensor(uint8_t index)
{
    return sensor_list[index]->read();
}

void SensorManager::parseGetSensorInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet) 
{
    uint8_t index = in_packet->read();
    sensor_info_t * info = sensor_list[index]->getSensorInfo();

    out_packet->command = COMMAND_SENSOR_REPORT;
    out_packet->data[0] = index;
    out_packet->data[1] = info->type;

    for (uint8_t i = 0; i < 60; i++) {
        out_packet->data[2 + i] = info->name[i];
        if (info->name[i] == '\0')
        {
            return;
        }
    }
}

void SensorManager::parseGetSensor(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t index = in_packet->read();
    sensor_info_t * info = sensor_list[index]->getSensorInfo();
    uint16_t sensor_value = sensor_list[index]->read();

    out_packet->command = COMMAND_SENSOR_REPORT;
    out_packet->data[0] = index;
    out_packet->data[1] = info->type;
    out_packet->data[2] = sensor_value & 0xFF;
    out_packet->data[3] = (sensor_value >> 8) & 0xFF;
}