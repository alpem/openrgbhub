/*

zone.hpp

Interface for Zone objects

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "usbmodule.hpp"
#include "effect.hpp"

class Zone
{
public:
    /* Methods to implement */
    virtual void begin() = 0;
    virtual void update() = 0;

    virtual void setLED(uint8_t index, uint8_t red, uint8_t green, uint8_t blue) = 0;    
    virtual uint8_t getLEDR(uint8_t index) = 0;
    virtual uint8_t getLEDG(uint8_t index) = 0;
    virtual uint8_t getLEDB(uint8_t index) = 0;

    /* Overridable methods */
    virtual void clear();

    /* Built-in methods */
    void resize(uint8_t new_size);

    zone_info_t * getInfo();

    Effect* getEffect();
    void    setEffect(Effect * effect);
    void    updateEffect();

protected:
    zone_info_t info;

    Effect * effect;
};