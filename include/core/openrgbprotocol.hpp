/*

openrgbprotocol.hpp

Defines for the openrgb protocol.

Author: Pol Rius (alpemwarrior)

*/

#pragma once 

#include <stdint.h>

#define OPENRGB_PROTOCOL_STANDARD_SIZE 64

#define COMMAND_DEVICE_REPORT 0xD0
#define COMMAND_ZONE_REPORT   0xD1
#define COMMAND_EFFECT_REPORT 0xD2
#define COMMAND_FAN_REPORT    0xD3
#define COMMAND_SENSOR_REPORT 0xD4

#define COMMAND_SET_EFFECT    0xE0
#define COMMAND_SET_LED       0XE1
#define COMMAND_FORCE_UPDATE  0xE2
#define COMMAND_RESIZE_ZONE   0xE3
#define COMMAND_GET_LED       0xE4
#define COMMAND_GET_EFFECT    0xE5
#define COMMAND_SET_FAN_FIXED 0xE6
#define COMMAND_GET_SENSOR    0xE7
#define COMMAND_SET_FAN_STATE 0xE8
#define COMMAND_GET_FAN_STATE 0xE9
#define COMMAND_GET_FAN_FEEDBACK 0xEA

#define EFFECT_FLAGS_HAS_SPEED        0b10000000 
#define EFFECT_FLAGS_HAS_BRIGHTNESS   0b01000000 
#define EFFECT_FLAGS_HAS_DIRECTION_LR 0b00100000 
#define EFFECT_FLAGS_HAS_DIRECTION_UD 0b00010000
#define EFFECT_FLAGS_HAS_DIRECTION_HV 0b00001000

#define EFFECT_FLAGS_HAS_MODE_SPECIFIC_COLORS 0b01000000
#define EFFECT_FLAGS_HAS_MODE_RANDOM_COLORS   0b10000000

#define ZONE_FLAGS_HAS_RESIZING 0b10000000

#define FAN_FLAGS_HAS_PWM_MODE     0b10000000
#define FAN_FLAGS_HAS_DC_MODE      0b01000000
#define FAN_FLAGS_HAS_RPM_FEEDBACK 0b00100000
#define FAN_FLAGS_HAS_RPM_SETPOINT 0b00010000

#define DEVICE_VERSION(MAJOR, MINOR) (((MAJOR << 4) & 0xF0) | (MINOR & 0x0F))

enum
{
    ZONE_TYPE_SINGLE = 0,
    ZONE_TYPE_LINEAR = 1
};

enum 
{
    EFFECT_PARAM_COLOR_MODE_NONE     = 0x00,
    EFFECT_PARAM_COLOR_MODE_SPECIFIC = 0x01,
    EFFECT_PARAM_COLOR_MODE_RANDOM   = 0x02,
};

enum
{
    MODE_PARAM_DIRECTION_LEFT         = 0,        /* Mode direction left              */
    MODE_PARAM_DIRECTION_RIGHT        = 1,        /* Mode direction right             */
    MODE_PARAM_DIRECTION_UP           = 2,        /* Mode direction up                */
    MODE_PARAM_DIRECTION_DOWN         = 3,        /* Mode direction down              */
    MODE_PARAM_DIRECTION_HORIZONTAL   = 4,        /* Mode direction horizontal        */
    MODE_PARAM_DIRECTION_VERTICAL     = 5,        /* Mode direction vertical          */
};

enum 
{   
    SENSOR_TYPE_TEMP = 0,
    SENSOR_TYPE_VOLTAGE = 1
};

enum
{
    FAN_MODE_ABSOLUTE = 0,
    FAN_MODE_RPM = 1
};

enum 
{
    FAN_FUNCTION_FIXED = 0,
    FAN_FUNCTION_CURVE = 1
};

/* Struct to simplify accessing packed RGB color data */
typedef struct __attribute__ ((packed))
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
}RGB_color_t;

typedef struct 
{
    unsigned char * name;
    uint8_t         type;
    uint8_t         features = 0;
    uint8_t         size_max;
    uint8_t         size;
}zone_info_t;

/* Helper struct that contains the info required for COMMAND_EFFECT_REPORT */
typedef struct __attribute__ ((packed))
{
    uint8_t feature_bitfield;
    uint8_t supported_modes;
    uint8_t min_speed;
    uint8_t max_speed;
    uint8_t min_brightness;
    uint8_t max_brightness;
    uint8_t color_array_min;
    uint8_t color_array_max;
    const char * name;
}effect_info_t;

/* Helper struct that contains the info provided by COMMAND_SET_EFFECT and COMMAND_GET_EFFECT */
typedef struct __attribute__ ((packed))
{
    uint8_t index;
    uint8_t color_mode;
    uint8_t speed;
    uint8_t direction_bitfield;
    uint8_t brightness;
    uint8_t color_array_size;
    RGB_color_t color_array[18];
}effect_param_t;

typedef struct 
{
    uint8_t features = 0;
    const char * name;
}fan_info_t;

typedef struct 
{
    uint16_t sensor_val;
    uint16_t fan_val;
}fan_curve_datapoint_t;

typedef struct 
{
    uint8_t type;
    const char * name;
}sensor_info_t;

typedef struct 
{
    uint16_t speed;
    uint8_t mode;
}fan_speed_t;

typedef struct 
{
    uint8_t sensor_index;
    uint8_t data_array_size;
    fan_curve_datapoint_t data_array[14];
}fan_curve_param_t;

typedef struct 
{
    uint8_t function; // Fixed or curve
    uint8_t fan_mode; // Absolute % or RPM

    /* Fixed function */
    uint16_t fixed_speed;

    /* Curve function */
    uint8_t sensor_index;
    uint8_t curve_sensor_index;
    uint8_t curve_array_size;
    fan_curve_datapoint_t curve_array[14];
}fan_state_t;