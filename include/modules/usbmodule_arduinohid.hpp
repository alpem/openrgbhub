/*

usbmodule_arduinohid.hpp

Implementation of the usbmodule using a modified version of NicoHood's HIDRaw library. 

Author: Pol Rius (alpemwarrior)

*/

#ifdef USE_ARDUINO_HID_MODULE

#pragma once

#include "usbmodule.hpp"

class ArduinoHIDUSBModule : public USBModule
{
public:
    void begin(hid_read_data_t * read_packet, hid_write_data_t * write_packet);
    bool readPacket(hid_read_data_t * packet);
    void writePacket(hid_write_data_t * packet);

private:
    uint8_t buffer[OPENRGB_PROTOCOL_STANDARD_SIZE];

    int bytes_read;
};

#endif