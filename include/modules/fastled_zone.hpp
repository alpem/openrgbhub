/*

fastled_zone.hpp

Zone implementation for FastLED-driven led strips. 

Author: Pol Rius (alpemwarrior)

*/

#ifdef USE_FASTLED_ZONE

#pragma once

#include "utils.hpp"
#include "zone.hpp"
#include <FastLED.h>

#define FASTLED_ZONE_UPDATE_HYSTERESIS 2

template <uint8_t P, uint8_t S>
class FastLEDZone : public Zone
{
public:
    FastLEDZone <P, S>(const char * name)
    {
        info.name       = (uint8_t*)name;
        info.size_max   = S;
        info.type       = ZONE_TYPE_LINEAR;
        info.features  |= ZONE_FLAGS_HAS_RESIZING;
        resize(S);
    };

    void beginWrapper()
    {
        FastLED.addLeds<NEOPIXEL, P>(led_array, S);
    };

    void begin()
    {
        beginWrapper();
        last_update = getTick();
    };

    void setLED(uint8_t index, uint8_t red, uint8_t green, uint8_t blue)
    {
        led_array[index].setRGB(red, green, blue);
    };
    
    uint8_t getLEDR(uint8_t index)
    {
        return led_array[index].r;
    };
    uint8_t getLEDG(uint8_t index)
    {
        return led_array[index].g;
    };
    uint8_t getLEDB(uint8_t index)
    {
        return led_array[index].b;
    };

    void update(){
        commonUpdate();
    };

private:
    static uint32_t last_update;
    static void commonUpdate()
    {
        /* Avoid updating twice */
        if ((getTick() - last_update) < FASTLED_ZONE_UPDATE_HYSTERESIS) return;
        FastLED.show();
        last_update = getTick();
        return;
    };

    CRGB led_array[S];
};

template <uint8_t P, uint8_t S>
uint32_t FastLEDZone<P, S>::last_update = 0;

#endif